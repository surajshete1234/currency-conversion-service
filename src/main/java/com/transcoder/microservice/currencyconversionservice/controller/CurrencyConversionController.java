package com.transcoder.microservice.currencyconversionservice.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.transcoder.microservice.currencyconversionservice.bean.CurrencyConversion;
import com.transcoder.microservice.currencyconversionservice.proxy.CurrencyExchangeProxy;

@RestController
@RequestMapping("/currency-conversion")
public class CurrencyConversionController {
	
	
	@Autowired
	private CurrencyExchangeProxy conversionproxy;
	
	
	@GetMapping("/from/{from}/to/{to}/{quantity}")
	public CurrencyConversion calculateCurrencyConversion(
			@PathVariable String from,@PathVariable String to,@PathVariable BigDecimal quantity) {
		
		HashMap<String,String> uriVariables = new HashMap<>();
		uriVariables.put("from", from);
		uriVariables.put("to", to);
		
		ResponseEntity<CurrencyConversion> responseEntity = 
				new RestTemplate().
				getForEntity("http://localhost:9000/currency-exchange/from/{from}/to/{to}", CurrencyConversion.class,uriVariables );
		
		CurrencyConversion currencyConversion = responseEntity.getBody();
		
		currencyConversion.setTotalCalculatedAmount(quantity.multiply(currencyConversion.getConversionMultiple()));
		
		currencyConversion.setQuantity(quantity);
		
		return currencyConversion;
	}
	
	@GetMapping("/converionMorethan19")
	public List<CurrencyConversion> calculateCurrencyConversionMorethan19() {
		
		ResponseEntity<List<CurrencyConversion>> responseEntity = new RestTemplate().
				exchange("http://localhost:9000/currency-exchange/fetchMoreThan19",  
				HttpMethod.GET,
				null,
				new ParameterizedTypeReference<List<CurrencyConversion>>() {});
		
		List<CurrencyConversion> listConversion = responseEntity.getBody();
		
		for (CurrencyConversion con : listConversion) {
			con.setConversionMultiple(con.getConversionMultiple().multiply(BigDecimal.valueOf(2)));
			con.setTotalCalculatedAmount(BigDecimal.valueOf(5).multiply(con.getConversionMultiple()));
			con.setQuantity(BigDecimal.valueOf(5));
		}
		
		return listConversion;
	}
	
	@GetMapping("/converionMorethan19Feign")
	public List<CurrencyConversion> calculateCurrencyConversionMorethan19Feign() {
		
		List<CurrencyConversion> listConversion = conversionproxy.fetchMoreThan19();
		
		for (CurrencyConversion con : listConversion) {
			con.setConversionMultiple(con.getConversionMultiple().multiply(BigDecimal.valueOf(2)));
			con.setTotalCalculatedAmount(BigDecimal.valueOf(5).multiply(con.getConversionMultiple()));
			con.setQuantity(BigDecimal.valueOf(5));
		}
		
		return listConversion;
	}
	
	
	@GetMapping("feign/from/{from}/to/{to}/{quantity}")
	public CurrencyConversion calculateCurrencyConversionfeign(
			@PathVariable String from,@PathVariable String to,@PathVariable BigDecimal quantity) {
		
		CurrencyConversion currencyConversion = conversionproxy.convert(from, to);
		currencyConversion.setTotalCalculatedAmount(quantity.multiply(currencyConversion.getConversionMultiple()));
		currencyConversion.setQuantity(quantity);
		return currencyConversion;
	}
	

}
