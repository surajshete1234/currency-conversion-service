package com.transcoder.microservice.currencyconversionservice.proxy;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.transcoder.microservice.currencyconversionservice.bean.CurrencyConversion;

@Component
//@FeignClient(name="currency-exchange",url = "localhost:9000")
@FeignClient(name="currency-exchange")
public interface CurrencyExchangeProxy {
	
	@GetMapping("currency-exchange/fetchMoreThan19")
	public List<CurrencyConversion> fetchMoreThan19 ();
	
	@GetMapping("currency-exchange/from/{from}/to/{to}")
	public CurrencyConversion convert (@PathVariable String from,
			@PathVariable String to) ;

}
